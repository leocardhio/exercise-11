import mongoose from 'mongoose';
import transactionSchema from '../../src/schemas/transactionSchema';
import customerSchema from '../../src/schemas/customerSchema';

describe('transactionSchema', () => {
  describe('^getActualAmount', () => {
    it('should return -5000 when invoked from a transaction document which has WITHDRAW type and amount of 5000', () => {
      const CustomerModel = mongoose.model('Customer', customerSchema);
      const customerDocument = new CustomerModel({ name: 'Iwan' });
      const TransactionModel = mongoose.model('Transaction', transactionSchema);
      const transactionDocument = new TransactionModel({
        customer: customerDocument,
        amount: 5000,
        type: 'WITHDRAW'
      });
      const expectedResult = -5000;

      const actualResult = transactionDocument.getActualAmount();

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
