import mongoose from 'mongoose';
import walletSchema from '../../src/schemas/walletSchema';

describe('walletSchema', () => {
  describe('^addBalance', () => {
    it('should return 3000 when invoked with amount -5000 and initially had 8000 in balance', () => {
      const WalletModel = mongoose.model('Wallet', walletSchema);
      const walletDocument = new WalletModel({ balance: 8000 });
      const expectedResult = 3000;

      walletDocument.addBalance(-5000);

      const actualResult = walletDocument.balance;
      expect(actualResult).toEqual(expectedResult);
    });
  });
});
