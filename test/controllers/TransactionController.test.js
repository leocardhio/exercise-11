import 'dotenv/config';
import request from 'supertest';
import mongoose from 'mongoose';
import { StatusCodes } from 'http-status-codes';
import app from '../../src/app';
import transactionSchema from '../../src/schemas/transactionSchema';
import walletSchema from '../../src/schemas/walletSchema';

let transactionModel;
let walletModel;

beforeAll(() => {
  mongoose.connect(process.env.DB_URL);
});

afterAll(async () => {
  await mongoose.disconnect();
});

beforeEach(async () => {
  transactionModel = mongoose.model('Transaction', transactionSchema);
  walletModel = mongoose.model('Wallet', walletSchema);
});

afterEach(async () => {
  await transactionModel.deleteMany();
  await walletModel.deleteMany();
});

describe('TransactionController', () => {
  describe('POST /wallets/:id/transactions', () => {
    it('should response with status code 201 and the transaction document', async () => {
      const expectedResultCode = StatusCodes.CREATED;
      const walletDetail = { balance: 4000 };
      const walletDocument = await walletModel.create(walletDetail);
      const newTransactionDetail = {
        amount: 3000000000,
        description: 'income',
        type: 'DEPOSIT'
      };
      const { id: walletId } = walletDocument;

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .post(`/wallets/${walletId}/transactions`)
        .send(newTransactionDetail);

      const expectedBody = await transactionModel.findById(actualBody.id);
      const actualResultBodyJSON = JSON.parse(JSON.stringify(actualBody));
      const expectedResultBodyJSON = JSON.parse(JSON.stringify(expectedBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBodyJSON).toEqual(expectedResultBodyJSON);
    });

    it('should respond with status code of 400 and error message \'"date" is not allowed\' when customer provides date', async () => {
      const expectedResultCode = StatusCodes.BAD_REQUEST;
      const walletDetail = { balance: 4000 };
      const walletDocument = await walletModel.create(walletDetail);
      const { id: walletId } = walletDocument;
      const newTransactionDetail = {
        date: '2019-07-30T06:43:40.252Z',
        amount: 3000000000,
        description: 'income',
        type: 'DEPOSIT'
      };
      const expectedResultBody = { error: '"date" is not allowed' };

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .post(`/wallets/${walletId}/transactions`)
        .send(newTransactionDetail);

      const actualResultBody = JSON.parse(JSON.stringify(actualBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBody).toEqual(expectedResultBody);
    });
    it("should respond with status code of 400 and error message 'withdraw amount exceed available balance' when customer tries to withdraw excessively", async () => {
      const expectedResultCode = StatusCodes.BAD_REQUEST;
      const walletDetail = { balance: 4000 };
      const walletDocument = await walletModel.create(walletDetail);
      const { id: walletId } = walletDocument;
      await transactionModel.create({
        walletId,
        amount: 500,
        type: 'DEPOSIT'
      });
      const newTransactionDetail = {
        amount: 30000,
        description: 'income',
        type: 'WITHDRAW'
      };
      const expectedResultBody = {
        error: 'withdraw amount exceed available balance'
      };

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .post(`/wallets/${walletId}/transactions`)
        .send(newTransactionDetail);

      const actualResultBody = JSON.parse(JSON.stringify(actualBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBody).toEqual(expectedResultBody);
    });
  });

  describe('GET /wallets/:id/transactions', () => {
    it('should respond with status code 200 and list of transactions of a certain customer', async () => {
      const expectedStatusCode = StatusCodes.OK;
      const walletDetail = { balance: 3000 };
      const walletDocument = await walletModel.create(walletDetail);
      const { id: walletId } = walletDocument;
      const transactionDetail = {
        walletId,
        amount: 5000,
        description: 'income',
        type: 'DEPOSIT'
      };
      const transactionDocument =
        await transactionModel.create(transactionDetail);
      const expectedResult = { transactions: [transactionDocument] };
      const expectedResultJSON = JSON.parse(JSON.stringify(expectedResult));

      const { statusCode: actualStatusCode, body: actualBody } = await request(
        app
      ).get(`/wallets/${walletId}/transactions`);

      const actualBodyJSON = JSON.parse(JSON.stringify(actualBody));
      expect(actualStatusCode).toBe(expectedStatusCode);
      expect(actualBodyJSON).toEqual(expectedResultJSON);
    });
  });
});
