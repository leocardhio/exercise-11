import 'dotenv/config';
import gql from 'graphql-tag';
import mongoose from 'mongoose';
import request from 'supertest-graphql';
import app from '../../src/app';
import customerSchema from '../../src/schemas/customerSchema';
import walletSchema from '../../src/schemas/walletSchema';

let customerModel;

let walletModel;

beforeAll(() => {
  mongoose.connect(process.env.DB_URL);
});

afterAll(async () => {
  await mongoose.disconnect();
});

beforeEach(async () => {
  customerModel = mongoose.model('Customer', customerSchema);
  walletModel = mongoose.model('Wallet', walletSchema);
});

afterEach(async () => {
  await customerModel.deleteMany();
  await walletModel.deleteMany();
});

describe('GraphQLController', () => {
  describe('QueryCustomer', () => {
    it('should return customer name when query is request only name', async () => {
      const walletDocument = await walletModel.create({ balance: 0 });
      const newCustomerDetail = {
        name: 'Cecep Warmindo',
        wallet: walletDocument
      };
      const newCustomerDocument = await customerModel.create(newCustomerDetail);
      const { id: customerId } = newCustomerDocument;

      const response = await request(app).query(gql`
        query {
          customer(id: "${customerId}") {
            name
          }
        }
      `);

      expect(response.errors).toBeUndefined();
      expect(response.data.customer.name).toEqual(newCustomerDocument.name);
    });

    it('should contain errors when the query request invalid property', async () => {
      const walletDocument = await walletModel.create({ balance: 0 });
      const newCustomerDetail = {
        name: 'Cecep Warmindo',
        wallet: walletDocument
      };
      const newCustomerDocument = await customerModel.create(newCustomerDetail);
      const { id: customerId } = newCustomerDocument;
      const invalidProperty = 'invalid';

      const response = await request(app).query(gql`
        query {
          customer(id: "${customerId}") {
            ${invalidProperty}
          }
        }
      `);

      expect(response.errors).toBeDefined();
    });
  });

  describe('Mutation createCustomer', () => {
    it('should create new customer and respond with new customers', async () => {
      const newCustomerDetail = {
        name: 'Cecep Warmindo'
      };

      const response = await request(app).mutate(gql`
      mutation {
        createCustomer(name: "${newCustomerDetail.name}") {
          id
          name
        }
      }
    `);

      const createdCustomer = await customerModel
        .findById(response.data.createCustomer.id)
        .populate('wallet');

      expect(createdCustomer.name).toEqual(newCustomerDetail.name);
    });
  });
});
