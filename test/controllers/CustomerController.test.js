import 'dotenv/config';
import request from 'supertest';
import mongoose from 'mongoose';
import { StatusCodes } from 'http-status-codes';
import app from '../../src/app';
import customerSchema from '../../src/schemas/customerSchema';
import walletSchema from '../../src/schemas/walletSchema';

let customerModel;

let walletModel;

beforeAll(() => {
  mongoose.connect(process.env.DB_URL);
});

afterAll(async () => {
  await mongoose.disconnect();
});

beforeEach(async () => {
  customerModel = mongoose.model('Customer', customerSchema);
  walletModel = mongoose.model('Wallet', walletSchema);
});

afterEach(async () => {
  await customerModel.deleteMany();
  await walletModel.deleteMany();
});

describe('CustomerController', () => {
  describe('GET /customers/:id', () => {
    it('should respond with status code of 200 and a customer object', async () => {
      const expectedResultCode = StatusCodes.OK;
      const walletDocument = await walletModel.create({ balance: 0 });
      const newCustomerDetail = {
        name: 'Cecep Warmindo',
        wallet: walletDocument
      };
      const newCustomerDocument = await customerModel.create(newCustomerDetail);
      const expectedResultBody = JSON.parse(
        JSON.stringify(newCustomerDocument)
      );
      const { id: customerId } = newCustomerDocument;

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      ).get(`/customers/${customerId}`);

      const actualResultBody = JSON.parse(JSON.stringify(actualBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBody).toEqual(expectedResultBody);
    });

    it('should respond with status code of 404 when using a nonexistent customer id', async () => {
      const expectedResultCode = StatusCodes.NOT_FOUND;
      const { statusCode: actualStatusCode } = await request(app).get(
        `/customers/662bcb86991864d29b027ae2`
      );

      expect(actualStatusCode).toBe(expectedResultCode);
    });
  });

  describe('PATCH /customers/:id', () => {
    it('should respond with status code of 204, has empty body, and the old name value is updated', async () => {
      const expectedResultCode = StatusCodes.NO_CONTENT;
      const walletDocument = await walletModel.create({ balance: 0 });
      const newCustomerDetail = {
        name: 'Cecep Warmindo',
        wallet: walletDocument
      };
      const newCustomerDocument = await customerModel.create(newCustomerDetail);
      const updateData = { name: 'Yayan Sedot WC' };
      newCustomerDocument.name = updateData.name;
      const expectedNewDocumentJSON = JSON.parse(
        JSON.stringify(newCustomerDocument)
      );
      const { id: customerId } = newCustomerDocument;

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .patch(`/customers/${customerId}`)
        .send(updateData);

      const actualResultBody = JSON.parse(JSON.stringify(actualBody));
      const emptyBody = {};
      const actualNewDocument = await customerModel
        .findById(customerId)
        .populate('wallet');
      const actualNewDocumentJSON = JSON.parse(
        JSON.stringify(actualNewDocument)
      );
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBody).toEqual(emptyBody);
      expect(actualNewDocumentJSON).toEqual(expectedNewDocumentJSON);
    });
    it("should respond with status code of 400 and error message '\"name\" must be a string' when customer doesn't provide name", async () => {
      const expectedResultCode = StatusCodes.BAD_REQUEST;
      const walletDocument = await walletModel.create({ balance: 0 });
      const newCustomerDetail = {
        name: 'Cecep Warmindo',
        wallet: walletDocument
      };
      const newCustomerDocument = await customerModel.create(newCustomerDetail);
      const { id: customerId } = newCustomerDocument;
      const expectedResultBody = { error: '"name" must be a string' };

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .patch(`/customers/${customerId}`)
        .send({ name: 123 });

      const actualResultBody = JSON.parse(JSON.stringify(actualBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBody).toEqual(expectedResultBody);
    });
  });

  describe('POST /customers', () => {
    it('should respond with status code of 201 and the new customers', async () => {
      const expectedResultCode = StatusCodes.CREATED;
      const newCustomerDetail = {
        name: 'Cecep Warmindo'
      };

      const { body: actualBody, statusCode: actualStatusCode } = await request(
        app
      )
        .post(`/customers`)
        .send(newCustomerDetail);

      const expectedBody = await customerModel
        .findById(actualBody.id)
        .populate('wallet');
      const actualResultBodyJSON = JSON.parse(JSON.stringify(actualBody));
      const expectedResultBodyJSON = JSON.parse(JSON.stringify(expectedBody));
      expect(actualStatusCode).toBe(expectedResultCode);
      expect(actualResultBodyJSON).toEqual(expectedResultBodyJSON);
    });
  });
});
