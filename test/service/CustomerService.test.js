import CustomerService from '../../src/services/CustomerService';

const mockWallet = {
  id: 1,
  balance: 0
};

describe('CustomerService', () => {
  const customerModel = {
    findById: jest.fn(),
    updateOne: jest.fn(),
    create: jest.fn()
  };

  const walletService = {
    add: jest.fn()
  };
  describe('^getById', () => {
    it('should get a customer', async () => {
      const expectedResult = {
        id: 1,
        name: 'Asep',
        wallet: mockWallet
      };
      customerModel.findById.mockImplementation(() => ({
        populate: jest.fn().mockResolvedValue(expectedResult)
      }));
      const customerService = new CustomerService(customerModel);

      const actualResult = await customerService.getById(1);
      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('^updateById', () => {
    it('should update a customer without returning the document', async () => {
      customerModel.updateOne.mockResolvedValue({});
      const customerService = new CustomerService(customerModel);

      const actualResult = await customerService.updateById(1);

      expect(actualResult).toEqual({});
    });
  });

  describe('^add', () => {
    it('should add new customer', async () => {
      const expectedResult = {
        id: 1,
        name: 'Asep',
        wallet: mockWallet
      };
      customerModel.create.mockResolvedValue(expectedResult);
      walletService.add.mockResolvedValue(mockWallet);
      const addWalletParameter = { balance: mockWallet.balance };
      const customerService = new CustomerService(customerModel, walletService);

      const actualResult = await customerService.add({ name: 'Asep' });

      expect(actualResult).toEqual(expectedResult);
      expect(walletService.add).toHaveBeenCalledTimes(1);
      expect(walletService.add).toHaveBeenCalledWith(addWalletParameter);
    });
  });
});
