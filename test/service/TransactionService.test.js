import TransactionService from '../../src/services/TransactionService';

const transactions = [
  {
    wallet: {
      id: 1,
      balance: 9000
    },
    amount: 999,
    description: 'income',
    type: 'DEPOSIT'
  }
];

const mockWallet = {
  id: 1,
  balance: 9000
};

describe('TransactionService', () => {
  const walletService = {
    getById: jest.fn(),
    addBalanceTo: jest.fn()
  };
  const transactionModel = {
    create: jest.fn(),
    find: jest.fn(),
    populate: jest.fn()
  };

  describe('^add', () => {
    it('should create new transaction and returns it', async () => {
      const [firstTransaction] = transactions;
      const expectedResult = firstTransaction;
      const newTransaction = {
        wallet: 1,
        amount: 999,
        description: 'income',
        type: 'DEPOSIT'
      };
      transactionModel.create.mockResolvedValue(expectedResult);
      walletService.getById.mockResolvedValue(mockWallet);
      walletService.addBalanceTo.mockResolvedValue({
        id: 1,
        balance: 9999,
        save: jest.fn()
      });
      const transactionService = new TransactionService(
        transactionModel,
        walletService
      );

      const actualResult = await transactionService.add(newTransaction);

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('^fetchAllByWalletId', () => {
    it("should return list of a certain customer's transactions", async () => {
      const [firstTransaction] = transactions;
      const expectedResult = [firstTransaction];
      transactionModel.find.mockResolvedValue(transactions);
      const transactionService = new TransactionService(
        transactionModel,
        walletService
      );

      const actualResult = await transactionService.fetchAllByWalletId(1);

      expect(actualResult).toEqual(expectedResult);
    });
  });
});
