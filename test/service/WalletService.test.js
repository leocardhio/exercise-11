import InsufficientBalanceError from '../../src/errors/InsufficientBalanceError';
import WalletService from '../../src/services/WalletService';

const baseWallet = {
  id: 1,
  balance: 1000
};

describe('WalletService', () => {
  const walletModel = {
    create: jest.fn(),
    findById: jest.fn()
  };

  describe('^add', () => {
    it('should create new wallet and returns it', async () => {
      const mockWallet = { ...baseWallet, addBalance: jest.fn() };
      walletModel.create.mockResolvedValue(mockWallet);
      const expectedResult = mockWallet;
      const walletService = new WalletService(walletModel);

      const actualResult = await walletService.add(mockWallet);

      expect(actualResult).toEqual(expectedResult);
    });
  });

  describe('^addBalanceTo', () => {
    it('should add balance to existing wallet when invoked with its corresponding id', async () => {
      const mockWallet = { ...baseWallet, addBalance: jest.fn() };
      walletModel.findById.mockResolvedValue(mockWallet);
      const addedAmount = 3000;
      mockWallet.addBalance.mockImplementation(() => {
        mockWallet.balance += addedAmount;
      });
      const expectedResult = { ...mockWallet };
      expectedResult.balance = 4000;
      const walletService = new WalletService(walletModel);

      const actualResult = await walletService.addBalanceTo(
        mockWallet,
        addedAmount
      );

      expect(actualResult).toEqual(expectedResult);
      expect(mockWallet.addBalance).toHaveBeenCalledTimes(1);
      expect(mockWallet.addBalance).toHaveBeenCalledWith(addedAmount);
    });

    it('should throw InsufficientBalanceError when tries to reduce balance more than available', async () => {
      const mockWallet = { ...baseWallet, addBalance: jest.fn() };
      walletModel.findById.mockResolvedValue(mockWallet);
      const addedAmount = -3000;
      mockWallet.addBalance.mockImplementation(() => {
        mockWallet.balance += addedAmount;
      });
      const walletService = new WalletService(walletModel);

      const actualResult = async () => {
        await walletService.addBalanceTo(mockWallet, addedAmount);
      };

      await expect(actualResult).rejects.toThrow(InsufficientBalanceError);
      expect(mockWallet.addBalance).toHaveBeenCalledTimes(1);
      expect(mockWallet.addBalance).toHaveBeenCalledWith(addedAmount);
    });
  });
});
