import { StatusCodes } from 'http-status-codes';

const dataValidationHelper =
  (schema, requestType, statusCode) => (request, response, next) => {
    const { error } = schema.validate(request[requestType]);
    if (error) {
      error.statusCode = statusCode;
      next(error);
    }
    next();
  };

const parameterDataValidationHelper = (schema) => {
  return dataValidationHelper(schema, 'params', StatusCodes.NOT_FOUND);
};
const bodyDataValidationHelper = (schema) => {
  return dataValidationHelper(schema, 'body', StatusCodes.BAD_REQUEST);
};

export { parameterDataValidationHelper, bodyDataValidationHelper };
