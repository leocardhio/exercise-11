import 'dotenv/config';
import express from 'express';
import { model } from 'mongoose';
import swaggerUI from 'swagger-ui-express';
import swaggerDocument from '../docs/WalletTransactionAPI.json';
import CustomerController from './controllers/CustomerController';
import GraphQLController from './controllers/GraphQLController';
import TransactionController from './controllers/TransactionController';
import errorMiddleware from './middlewares/errorMiddleware';
import initializeRoutes from './routes';
import customerSchema from './schemas/customerSchema';
import transactionSchema from './schemas/transactionSchema';
import walletSchema from './schemas/walletSchema';
import CustomerService from './services/CustomerService';
import TransactionService from './services/TransactionService';
import WalletService from './services/WalletService';

const app = express();
app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument));
app.use(express.json());

const createModels = () => {
  return {
    customerModel: model('Customer', customerSchema),
    transactionModel: model('Transaction', transactionSchema),
    walletModel: model('Wallet', walletSchema)
  };
};

const createServices = (models) => {
  const { customerModel, transactionModel, walletModel } = models;
  const walletService = new WalletService(walletModel);
  const customerService = new CustomerService(customerModel, walletService);
  const transactionService = new TransactionService(
    transactionModel,
    walletService
  );

  return {
    customerService,
    transactionService,
    walletService
  };
};

const createController = (services) => {
  const { customerService, transactionService } = services;
  return {
    graphQlController: new GraphQLController(customerService),
    customerController: new CustomerController(customerService),
    transactionController: new TransactionController(transactionService)
  };
};

const initializeDependencies = () => {
  const models = createModels();
  const services = createServices(models);
  app.locals.controllers = createController(services);
};

const main = async () => {
  initializeDependencies();
  initializeRoutes(app);
};

main();

app.use(errorMiddleware);

export default app;
