import CustomerNotFoundError from '../errors/CustomerNotFoundError';

export default class CustomerService {
  #customerModel;

  #walletService;

  constructor(customerModel, walletService) {
    this.#customerModel = customerModel;
    this.#walletService = walletService;
  }

  async getById(customerId) {
    const customer = await this.#customerModel
      .findById(customerId)
      .populate('wallet');
    if (!customer) {
      throw new CustomerNotFoundError();
    }

    return customer;
  }

  async updateById(customerId, updatedCustomerData) {
    await this.#customerModel.updateOne(
      { _id: customerId },
      updatedCustomerData
    );
    return {};
  }

  async add(customer) {
    const newCustomer = { ...customer };
    newCustomer.wallet = await this.#createNewWalletWithBalance(0);

    return this.#customerModel.create(newCustomer);
  }

  async #createNewWalletWithBalance(initialBalance) {
    const walletDetail = { balance: initialBalance };
    return this.#walletService.add(walletDetail);
  }
}
