export default class TransactionService {
  #transactionModel;

  #walletService;

  constructor(transactionModel, walletService) {
    this.#transactionModel = transactionModel;
    this.#walletService = walletService;
  }

  async add(transaction) {
    const newTransaction = { ...transaction };
    const { walletId } = newTransaction;
    const actualAmount = TransactionService.#getActualAmount(transaction);
    const wallet = await this.#walletService.addBalanceTo(
      walletId,
      actualAmount
    );
    newTransaction.walletId = wallet.id;
    const transactionDocument =
      await this.#transactionModel.create(newTransaction);
    await wallet.save();

    return transactionDocument;
  }

  static #getActualAmount(transaction) {
    const typeMapper = {
      WITHDRAW: -transaction.amount,
      DEPOSIT: transaction.amount
    };

    return typeMapper[transaction.type];
  }

  async fetchAllByWalletId(walletId) {
    return this.#transactionModel.find({
      walletId
    });
  }
}
