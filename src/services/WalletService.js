import InsufficientBalanceError from '../errors/InsufficientBalanceError';

export default class WalletService {
  #walletModel;

  constructor(walletModel) {
    this.#walletModel = walletModel;
  }

  async add(wallet) {
    return this.#walletModel.create(wallet);
  }

  async addBalanceTo(walletId, amount) {
    const wallet = await this.#walletModel.findById(walletId);

    wallet.addBalance(amount);
    if (wallet.balance < 0) {
      throw new InsufficientBalanceError();
    }

    return wallet;
  }
}
