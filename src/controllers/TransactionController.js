import { StatusCodes } from 'http-status-codes';

export default class TransactionController {
  #transactionService;

  constructor(transactionService) {
    this.#transactionService = transactionService;
    this.add = this.#add.bind(this);
    this.fetchAllByWalletId = this.#fetchAllByWalletId.bind(this);
  }

  async #add(request, response) {
    const { id: walletId } = request.params;
    const requestData = request.body;
    requestData.walletId = walletId;

    const transaction = await this.#transactionService.add(requestData);

    response.status(StatusCodes.CREATED).json(transaction);
  }

  async #fetchAllByWalletId(request, response) {
    const { id: walletId } = request.params;

    const transactions =
      await this.#transactionService.fetchAllByWalletId(walletId);

    response.status(StatusCodes.OK).json({ transactions });
  }
}
