import { StatusCodes } from 'http-status-codes';

export default class CustomerController {
  #customerService;

  constructor(customerService) {
    this.#customerService = customerService;
    this.getById = this.#getById.bind(this);
    this.updateById = this.#updateById.bind(this);
    this.add = this.#add.bind(this);
  }

  async #getById(request, response) {
    const { id: customerId } = request.params;

    const data = await this.#customerService.getById(customerId);

    response.status(StatusCodes.OK).json(data);
  }

  async #updateById(request, response) {
    const { id: customerId } = request.params;
    const updatedCustomerDetail = request.body;

    const data = await this.#customerService.updateById(
      customerId,
      updatedCustomerDetail
    );

    response.status(StatusCodes.NO_CONTENT).json(data);
  }

  async #add(request, response) {
    const requestData = request.body;

    const customer = await this.#customerService.add(requestData);
    response.status(StatusCodes.CREATED).json(customer);
  }
}
