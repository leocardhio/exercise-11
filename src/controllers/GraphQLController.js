import { createSchema } from 'graphql-yoga';
import typeDefs from '../graphql/typeDefs';

export default class GraphQLController {
  #customerService;

  constructor(customerService) {
    this.#customerService = customerService;
  }

  get schema() {
    return createSchema({
      typeDefs,
      resolvers: {
        Query: {
          customer: (_, args) => this.#customerService.getById(args.id)
        },
        Mutation: {
          createCustomer: (_, args) => this.#customerService.add(args)
        }
      }
    });
  }
}
