import { StatusCodes } from 'http-status-codes';

export default class InsufficientBalanceError extends Error {
  constructor() {
    super('withdraw amount exceed available balance');
    this.statusCode = StatusCodes.BAD_REQUEST;
  }
}
