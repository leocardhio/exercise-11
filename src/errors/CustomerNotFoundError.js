import { StatusCodes } from 'http-status-codes';

export default class CustomerNotFoundError extends Error {
  constructor() {
    super('customer resource not found');
    this.statusCode = StatusCodes.NOT_FOUND;
  }
}
