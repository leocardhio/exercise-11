import Joi from 'joi';

const walletValidationParameterSchema = Joi.object({
  id: Joi.string()
    .length(24)
    .required()
    .meta({
      _mongoose: { type: 'ObjectId', ref: 'Wallet' }
    })
});

export default walletValidationParameterSchema;
