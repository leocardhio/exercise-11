import Joi from 'joi';

const customerValidationBodySchemaForPost = Joi.object({
  name: Joi.string().required()
});

const customerValidationBodySchemaForPatch = Joi.object({
  name: Joi.string(),
  wallet: Joi.string()
    .length(24)
    .meta({
      _mongoose: { type: 'ObjectId', ref: 'Customer' }
    })
});

const customerValidationParameterSchema = Joi.object({
  id: Joi.string()
    .length(24)
    .required()
    .meta({
      _mongoose: { type: 'ObjectId', ref: 'Customer' }
    })
});

export {
  customerValidationBodySchemaForPost,
  customerValidationBodySchemaForPatch,
  customerValidationParameterSchema
};
