import Joi from 'joi';

const transactionValidationBodySchema = Joi.object({
  amount: Joi.number().positive().required(),
  description: Joi.string(),
  type: Joi.string().valid('DEPOSIT', 'WITHDRAW')
});

export default transactionValidationBodySchema;
