import cors from 'cors';
import express from 'express';
import { createYoga } from 'graphql-yoga';
import { parameterDataValidationHelper } from '../helpers/dataValidationHelper';
import walletValidationParameterSchema from '../validationSchema/walletValidationSchema';
import customerRoutes from './customers/customerRoutes';
import initializeTransactionRoutes from './wallets/:id/transactions/transactionRoutes';

const router = express.Router();

const initializeRoutes = (app) => {
  const { graphQlController } = app.locals.controllers;
  const yoga = createYoga({ schema: graphQlController.schema });

  app.use(yoga.graphqlEndpoint, yoga);
  app.use(
    cors({
      origin: ['http://localhost:3000', 'http://localhost:5173']
    })
  );
  app.use('/', router);
  router.use('/customers', customerRoutes(app));
  router.use(
    '/wallets/:id/transactions',
    parameterDataValidationHelper(walletValidationParameterSchema),
    initializeTransactionRoutes(app)
  );
};

export default initializeRoutes;
