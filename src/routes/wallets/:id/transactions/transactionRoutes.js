import { Router } from 'express';
import { bodyDataValidationHelper } from '../../../../helpers/dataValidationHelper';
import transactionValidationBodySchema from '../../../../validationSchema/transactionValidationSchema';
import asyncHelper from '../../../../helpers/asyncHelper';

const transactionRouter = Router({ mergeParams: true });

const transactionRoutes = (app) => {
  const { transactionController } = app.locals.controllers;

  transactionRouter.post(
    '/',
    bodyDataValidationHelper(transactionValidationBodySchema),
    asyncHelper(transactionController.add)
  );
  transactionRouter.get('/', transactionController.fetchAllByWalletId);

  return transactionRouter;
};

export default transactionRoutes;
