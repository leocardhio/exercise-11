import { Router } from 'express';
import { customerValidationBodySchemaForPatch } from '../../../validationSchema/customerValidationSchema';
import { bodyDataValidationHelper } from '../../../helpers/dataValidationHelper';
import asyncHelper from '../../../helpers/asyncHelper';

const routerCustomerWithId = Router({ mergeParams: true });

const customerRoutesWithId = (app) => {
  const { customerController } = app.locals.controllers;

  routerCustomerWithId.get('/', asyncHelper(customerController.getById));
  routerCustomerWithId.patch(
    '/',
    bodyDataValidationHelper(customerValidationBodySchemaForPatch),
    customerController.updateById
  );

  return routerCustomerWithId;
};

export default customerRoutesWithId;
