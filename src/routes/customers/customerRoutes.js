import { Router } from 'express';
import {
  customerValidationBodySchemaForPost,
  customerValidationParameterSchema
} from '../../validationSchema/customerValidationSchema';
import {
  bodyDataValidationHelper,
  parameterDataValidationHelper
} from '../../helpers/dataValidationHelper';
import initializeCustomerRoutesWithId from './:id/customerRoutesWithId';

const router = Router();

const initializeRoutes = (app) => {
  const { customerController } = app.locals.controllers;
  router.post(
    '/',
    bodyDataValidationHelper(customerValidationBodySchemaForPost),
    customerController.add
  );
  router.use(
    '/:id',
    parameterDataValidationHelper(customerValidationParameterSchema),
    initializeCustomerRoutesWithId(app)
  );
  return router;
};

export default initializeRoutes;
