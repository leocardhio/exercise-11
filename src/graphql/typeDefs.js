const typeDefs = `
type Customer{
  id: ID!,
  name: String!,
  wallet: Wallet!
}

type Wallet {
  id: ID!
  balance: Int!
}

input CustomerInput {
  name: String!
}

type Query {
  customer(id: ID!): Customer!
}

type Mutation {
  createCustomer(name: String!): Customer!
}
`;

export default typeDefs;
