import { Schema } from 'mongoose';

const transactionSchema = new Schema(
  {
    walletId: { type: Schema.Types.ObjectId, required: true },
    date: {
      type: String,
      default: new Date().toISOString(),
      required: true
    },
    amount: Number,
    description: String,
    type: {
      type: String,
      enum: ['WITHDRAW', 'DEPOSIT'],
      required: true
    }
  },
  {
    toJSON: {
      transform(document, returnValue) {
        const { _id, __v, ...rest } = returnValue;
        return { id: _id, ...rest };
      }
    },
    methods: {
      getActualAmount() {
        const typeMapper = {
          WITHDRAW: -this.amount,
          DEPOSIT: this.amount
        };

        return typeMapper[this.type];
      }
    }
  }
);

export default transactionSchema;
