import { Schema } from 'mongoose';

const customerSchema = new Schema(
  {
    name: String,
    wallet: { type: Schema.Types.ObjectId, ref: 'Wallet', required: true }
  },
  {
    toJSON: {
      transform(document, returnValue) {
        const { _id, __v, ...rest } = returnValue;
        return { id: _id, ...rest };
      }
    }
  }
);

export default customerSchema;
