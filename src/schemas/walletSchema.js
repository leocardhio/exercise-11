import { Schema } from 'mongoose';

const walletSchema = new Schema(
  {
    balance: Number
  },
  {
    toJSON: {
      transform(document, returnValue) {
        const { _id, __v, ...rest } = returnValue;
        return { id: _id, ...rest };
      }
    },
    methods: {
      addBalance(amount) {
        this.balance += amount;
      }
    }
  }
);

export default walletSchema;
