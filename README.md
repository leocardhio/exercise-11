# Wallet Transaction API

This API was built using ExpressJS and the goals was to enable customer to check their wallet balance and do transactions which limited to deposit and withdraw.

## Directory Structure

### Description

- **controllers/**: Contains controller files responsible for handling incoming requests and returning appropriate responses.
- **errors/**: Houses error handling logic and middleware for managing errors within the application.
- **helpers/**: Contains utility functions or helper modules used across different parts of the application.
- **middlewares/**: Holds middleware functions that intercept incoming requests before they reach the routes.
- **routes/**: Defines route handlers and endpoint configurations.
- **services/**: Contains service modules responsible for business logic and interacting with data models.

## Usage

1. Clone this repository.
2. Install dependencies using `npm install`.
3. Make a `.env` file according to your system, you can refer to the `.env.example` file
4. Start the development server with `npm start`.

## Important Notes

1. You can access swagger OAS by accessing the `/docs` url path
2. Make sure you run the server at the 3000 port
